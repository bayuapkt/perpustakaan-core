/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.co.id.entity;

import java.util.HashSet;
import java.util.Set;

public class Anggota {

    private int id;
    private String no; //no anggota
    private String nama;
    private String Telepon1;
    private String Telepon2;
    private Set<Telepon> telepons = new HashSet<>();

    public Anggota() {
    }

    public Anggota(int id) {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Set<Telepon> getTelepons() {
        return telepons;
    }

    public void setTelepons(Set<Telepon> telepons) {
        this.telepons = telepons;
    }

    @Override
    public String toString() {
        return "Anggota{" + "Id" + id + ", no=" + ", nama=" + ", telepons=" + telepons + '}';
    }

}
