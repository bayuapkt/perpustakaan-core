/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.co.id.entity;

/**
 *
 * @author Bayu
 */
public class Buku {
    private int id;
    private String Kode;
    private String Judul;
    private String Pengarang;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKode() {
        return Kode;
    }

    public void setKode(String Kode) {
        this.Kode = Kode;
    }

    public String getJudul() {
        return Judul;
    }

    public void setJudul(String Judul) {
        this.Judul = Judul;
    }

    public String getPengarang() {
        return Pengarang;
    }

    public void setPengarang(String Pengarang) {
        this.Pengarang = Pengarang;
    }

    @Override
    public String toString() {
        return "Buku{" + "id=" + id + ", Kode=" + Kode + ", Judul=" + Judul + ", Pengarang=" + Pengarang + '}';
    }
    
}
