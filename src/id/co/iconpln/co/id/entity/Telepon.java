/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.co.id.entity;

/**
 *
 * @author Bayu
 */
public class Telepon {

    private int id;
    private String no; //no telpon
    private Anggota anggota;

    public Telepon() {
    }

    public Telepon(int id, String no, Anggota anggota) {
        this.id = id;
        this.no = no;
        this.anggota = anggota;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Anggota getAnggota() {
        return anggota;
    }

    public void setAnggota(Anggota anggota) {
        this.anggota = anggota;
    }

    @Override
    public String toString() {
        return "Telepon{" + "id=" + id + ", no=" + no + '}';
    }

}
