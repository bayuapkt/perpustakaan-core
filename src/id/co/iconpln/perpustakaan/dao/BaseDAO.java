/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao;

import java.util.List;

/**
 *
 * @author Bayu
 * @param <T>
 */
public interface BaseDAO<T> {
    T save(T param);
    T update(T param);
    T delete(T param);
    T finById(T param);
    List<T> find(T param);
}
