/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao.impl;

import id.co.iconpln.co.id.entity.Anggota;
import id.co.iconpln.co.id.entity.Telepon;
import id.co.iconpln.perpustakaan.dao.AnggotaDAO;
import id.co.iconpln.perpustakaan.dao.TeleponDAO;
import id.co.iconpln.perpustakaan.service.ServiceDAO;
import id.co.iconpln.perpustakaan.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AnggotaDAOImpl implements AnggotaDAO {

    private final Connection connection = DBConnection.getInstance().getConnection();

    @Override
    public Anggota save(Anggota param) {
        String sql = "INSERT INTO anggota("
                + "no,"
                + "nama) "
                + "VALUES(?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, param.getNo());
            preparedStatement.setString(2, param.getNama());
            preparedStatement.executeUpdate();

            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs != null && rs.next()) {
                param.setId(rs.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AnggotaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return param;
    }

    @Override
    public Anggota update(Anggota param) {
        String sql = "update anggota set nama=? where no=?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, param.getNama());
            preparedStatement.setString(2, param.getNo());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnggotaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return param;
    }

    @Override
    public Anggota delete(Anggota param) {
        String sql = "delete from anggota where no=?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, param.getNo());
//            deleteTelepon(0);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnggotaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return param;
    }
    
    
    public void deleteTelepon(int idAnggota) {
        String sql = "delete from telepon where id_anggota=?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idAnggota);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnggotaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public Anggota finById(Anggota param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Anggota> find(Anggota param) {
        List<Anggota> result = new ArrayList<>();
        try {
            String sql = "SELECT * FROM anggota "
                    + "WHERE 1=1 ";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery(); 
            while (rs.next()) {
                Anggota anggota = new Anggota();
                anggota.setId(rs.getInt("id"));
                anggota.setNo(rs.getString("No"));
                anggota.setNama(rs.getString("Nama"));
                anggota.setTelepons(findTelepon(rs.getInt("id")));
                result.add(anggota);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(AnggotaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
     
    public Set<Telepon> findTelepon(int id_anggota) {
       Set<Telepon> result  = new HashSet<>();
        try {
            String sql = "select * from telepon "
                    + "where id_anggota=  "+id_anggota + " order by id";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery(); 
            while (rs.next()) {
                Telepon telepon = new Telepon(); 
                telepon.setNo(rs.getString("no"));
                telepon.setId(rs.getInt("id"));  
                result.add(telepon);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AnggotaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
     
}
