/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao.impl;

import id.co.iconpln.co.id.entity.Buku;
import id.co.iconpln.perpustakaan.dao.BukuDAO;
import id.co.iconpln.perpustakaan.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BukuDAOImpl implements BukuDAO {

    private final Connection connection = DBConnection.getInstance().getConnection();

    @Override
    public Buku save(Buku param) {
        String sql = "INSERT INTO buku("
                + "kode,"
                + "judul,"
                + "pengarang) "
                + "VALUES(?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, param.getKode());
            preparedStatement.setString(2, param.getJudul());
            preparedStatement.setString(3, param.getPengarang());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BukuDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return param;
    }

    @Override
    public Buku update(Buku param) {
        String sql = "update buku set judul=?,pengarang=? where kode=?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, param.getJudul());
            preparedStatement.setString(2, param.getPengarang());
            preparedStatement.setString(3, param.getKode());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BukuDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return param;
    }

    @Override
    public Buku delete(Buku param) {
        String sql = "delete from buku where kode=?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, param.getKode());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BukuDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return param;
    }

    @Override
    public Buku finById(Buku param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Buku> find(Buku param) {
        List<Buku> result = new ArrayList<>();
        try {
            String sql = "SELECT * FROM buku "
                    + "WHERE 1=1 ";

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Buku buku = new Buku();
                buku.setId(rs.getInt("id"));
                buku.setKode(rs.getString("kode"));
                buku.setJudul(rs.getString("judul"));
                buku.setPengarang(rs.getString("pengarang"));
                result.add(buku);

            }
        } catch (SQLException ex) {
            Logger.getLogger(BukuDAOImpl.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
