/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao.impl;

import id.co.iconpln.co.id.entity.Telepon;
import id.co.iconpln.perpustakaan.dao.TeleponDAO;
import id.co.iconpln.perpustakaan.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TeleponDAOImpl implements TeleponDAO {

    private final Connection connection = DBConnection.getInstance().getConnection();

    @Override
    public Telepon save(Telepon param) {
        String sql = "INSERT INTO telepon("
                + "no,"
                + "id_anggota)"
                + "VALUES(?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, param.getNo());
            preparedStatement.setInt(2, param.getAnggota().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TeleponDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return param;
    }

    @Override
    public Telepon update(Telepon param) {
        String sql = "update telepon set no=? where id=?";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, param.getNo());
            preparedStatement.setInt(2, param.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TeleponDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return param;
    }

    @Override
    public Telepon delete(Telepon param) {
        String sql = "delete from telepon where id =?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, param.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TeleponDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return param;
    }

    @Override
    public Telepon finById(Telepon param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Telepon> find(Telepon param) {

        List<Telepon> result = new ArrayList<>();
        try {
            String sql = "SELECT * FROM telepon "
                    + "WHERE id_anggota =  " + param.getId();

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Telepon telepon = new Telepon();
                telepon.setId(rs.getInt("id"));
                telepon.setNo(rs.getString("No"));
                result.add(telepon);

            }
        } catch (SQLException ex) {
            Logger.getLogger(TeleponDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
