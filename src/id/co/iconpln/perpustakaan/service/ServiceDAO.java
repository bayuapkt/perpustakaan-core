/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.service;

import id.co.iconpln.co.id.entity.Anggota;
import id.co.iconpln.co.id.entity.Buku;
import id.co.iconpln.co.id.entity.Telepon;
import id.co.iconpln.perpustakaan.dao.AnggotaDAO;
import id.co.iconpln.perpustakaan.dao.BukuDAO;
import id.co.iconpln.perpustakaan.dao.TeleponDAO;
import id.co.iconpln.perpustakaan.dao.impl.AnggotaDAOImpl;
import id.co.iconpln.perpustakaan.dao.impl.BukuDAOImpl;
import id.co.iconpln.perpustakaan.dao.impl.TeleponDAOImpl;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Bayu
 */
public class ServiceDAO {

    private final BukuDAO bukuDAO = new BukuDAOImpl();
    private final AnggotaDAO anggotaDAO = new AnggotaDAOImpl();
    private final TeleponDAO teleponDAO = new TeleponDAOImpl();

    public List<Buku> findBukus(Buku param) {
        return bukuDAO.find(param);
    }

    public Buku save(Buku param) {
        return bukuDAO.save(param);
    }

    public Buku update(Buku param) {
        return bukuDAO.update(param);
    }

    public Buku delete(Buku param) {
        return bukuDAO.delete(param);
    }

    public List<Anggota> findAnggotas(Anggota param) {
        return anggotaDAO.find(param);
    }

    public Anggota save(Anggota param) {
        Anggota result = anggotaDAO.save(param);
        for (Telepon telepon : param.getTelepons()) {
            telepon.setAnggota(result);
            teleponDAO.save(telepon);
        }
        return result;
    }

    public Anggota update(Anggota param) {
        return anggotaDAO.update(param);
    }

    public Anggota delete(Anggota param) {
        return anggotaDAO.delete(param);
    }

    public List<Telepon> findTelepons(Telepon param) {
        return teleponDAO.find(param);
//    }
//
//    public Telepon save(Telepon param) {
//        return teleponDAO.save(param);
    }

    public Telepon update(Telepon param) {
        return teleponDAO.update(param);
    }

    public Telepon delete(Telepon param) {
        return teleponDAO.delete(param);
    }
}
