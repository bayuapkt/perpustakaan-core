/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.view;

import id.co.iconpln.co.id.entity.Anggota;
import id.co.iconpln.co.id.entity.Telepon;
import id.co.iconpln.perpustakaan.service.ServiceDAO;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.table.TableModel;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Bayu
 */
public class AnggotaView extends JInternalFrame {

    private JTextField textFieldNo;
    private JTextField textFieldNama;
    private JTextField textFieldTelepon1;
    private JTextField textFieldTelepon2;
    private JTable table;
    private final List<Anggota> anggotas = new ArrayList<>();
    private TableAnggotaViewModel anggotaViewModel;
    private ServiceDAO serviceDAO = new ServiceDAO();
    private Anggota anggota;
    private final ButtonPanel buttonPanel;
    private Telepon t;
    private Set<Telepon> telepons = new HashSet<>();

    {
        loadAll();
    }

    private void loadAll() {
        anggotas.clear();
        anggotas.addAll(serviceDAO.findAnggotas(new Anggota()));

    }

    public AnggotaView(String string) {
        super(string);
        this.buttonPanel = new ButtonPanel() {
            @Override
            public ActionListener onButtonAddClick() {
                return new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int sizeTelepon = 0;
                        Telepon ts[] = new Telepon[2];
                        ts[0] = new Telepon();
                        ts[1] = new Telepon();
                        ts[0].setNo("");
                        ts[1].setNo("");

                        if (anggota != null) {
                            sizeTelepon = telepons.size();

                            if (sizeTelepon > 0) {
                                Iterator<Telepon> it = telepons.iterator();
                                ts[0] = it.next();
                                ts[1] = it.next();
                            }

                            anggota.setNo(textFieldNo.getText());
                            anggota.setNama(textFieldNama.getText());

                            ts[0].setAnggota(anggota);
                            ts[0].setNo(textFieldTelepon1.getText());
                            ts[1].setAnggota(anggota);
                            ts[1].setNo(textFieldTelepon2.getText());
                            telepons.clear();
                            telepons.add(ts[0]);
                            telepons.add(ts[1]);

                            anggota.setTelepons(telepons);
                            serviceDAO.update(anggota);
                            serviceDAO.update(ts[0]);
                            serviceDAO.update(ts[1]);
                            JOptionPane.showMessageDialog(rootPane, "EDIT BERHASIL");
                        } else {
                            anggota = new Anggota();
                            anggota.setNo(textFieldNo.getText());
                            anggota.setNama(textFieldNama.getText());
                            if (anggota.getNo().isEmpty()) {
                                JOptionPane.showMessageDialog(rootPane, "GAGAL, KODE KOSONG");
                            } else {

                                ts[0].setAnggota(anggota);
                                ts[0].setNo(textFieldTelepon1.getText());
                                ts[1].setAnggota(anggota);
                                ts[1].setNo(textFieldTelepon2.getText());
                                telepons.clear();
                                telepons.add(ts[0]);
                                telepons.add(ts[1]);

                                anggota.setTelepons(telepons);
                                serviceDAO.save(anggota);
                                JOptionPane.showMessageDialog(rootPane, "SAVE");
                            }

                        }
                        resetField();
                        loadAll();
                    }

                };
            }

            @Override

            public ActionListener onButtonEditClick() {
                return new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int sizeTelepon = 0;
                        Telepon ts[] = new Telepon[2];
                        ts[0] = new Telepon();
                        ts[1] = new Telepon();
                        ts[0].setNo("");
                        ts[1].setNo("");

                        if (anggota != null) {

                            sizeTelepon = telepons.size();

                            if (sizeTelepon > 0) {
                                Iterator<Telepon> it = telepons.iterator();
                                ts[0] = it.next();
                                ts[1] = it.next();
                            }

                            anggota.setNo(textFieldNo.getText());
                            anggota.setNama(textFieldNama.getText());

                            ts[0].setAnggota(anggota);
                            ts[0].setNo(textFieldTelepon1.getText());
                            ts[1].setAnggota(anggota);
                            ts[1].setNo(textFieldTelepon2.getText());
                            telepons.clear();
                            telepons.add(ts[0]);
                            telepons.add(ts[1]);

                            anggota.setTelepons(telepons);
                            serviceDAO.update(anggota);
                            serviceDAO.update(ts[0]);
                            serviceDAO.update(ts[1]);
                            JOptionPane.showMessageDialog(rootPane, "EDIT BERHASIL");
                        } else {
                            anggota = (Anggota) anggotaViewModel.getValueAt(table.getSelectedRow(), table.getColumnCount());
                            telepons = anggota.getTelepons();

                            sizeTelepon = telepons.size();

                            if (sizeTelepon > 0) {
                                Iterator<Telepon> it = telepons.iterator();
                                ts[0] = it.next();
                                ts[1] = it.next();
                            }

                            textFieldNo.setText(anggota.getNo());
                            textFieldNo.setEditable(false);
                            textFieldNama.setText(anggota.getNama());
                            textFieldTelepon1.setText(ts[0].getNo());
                            textFieldTelepon2.setText(ts[1].getNo());
                        }
                    }
                };
            }

            @Override
            public ActionListener onButtonDeleteClick() {
                return new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        if (table.getSelectedRow() < 0) {
                            JOptionPane.showMessageDialog(rootPane, "Data belum dipilih");
                        } else {
                            anggota = (Anggota) anggotaViewModel.getValueAt(table.getSelectedRow(), table.getColumnCount());
                            System.out.println("anggota = " + anggota);

                            Iterator<Telepon> it = anggota.getTelepons().iterator();
                            Telepon ts[] = new Telepon[2];
                            ts[0] = it.next();
                            ts[1] = it.next();
                            serviceDAO.delete(anggota);
                            serviceDAO.delete(ts[0]);
                            serviceDAO.delete(ts[1]);
                            resetField();
                            loadAll();
                            JOptionPane.showMessageDialog(rootPane, "DELETE BERHASIL");
                        }

                    }
                };
            }
        };
        initComponent();
    }

    private void initComponent() {
        textFieldNo = new JTextField();
        textFieldNo.setMinimumSize(new Dimension(200, 20));

        textFieldNama = new JTextField();
        textFieldNama.setMinimumSize(new Dimension(200, 20));

        textFieldTelepon1 = new JTextField();
        textFieldTelepon1.setMinimumSize(new Dimension(200, 20));

        textFieldTelepon2 = new JTextField();
        textFieldTelepon2.setMinimumSize(new Dimension(200, 20));

        anggotaViewModel = new TableAnggotaViewModel(anggotas);

        table = new JTable((TableModel) anggotaViewModel);
        table.setFillsViewportHeight(true);

        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new java.awt.Dimension(450, 150));

        MigLayout layout = new MigLayout("wrap 2");
        JPanel panelform = new JPanel();
        panelform.setLayout(layout);
        panelform.setBorder(new EtchedBorder(1));
        panelform.add(new JLabel("NO"));
        panelform.add(textFieldNo);
        panelform.add(new JLabel("NAMA"));
        panelform.add(textFieldNama);
        panelform.add(new JLabel("Telepon 1"));
        panelform.add(textFieldTelepon1);
        panelform.add(new JLabel("Telepon 2"));
        panelform.add(textFieldTelepon2);
        panelform.add(buttonPanel, "Span 2, align right");
        panelform.add(scrollPane, "Span 2");

        super.add(panelform);
        super.setPreferredSize(new Dimension(500, 400));
        super.setClosable(true);
        super.setMaximizable(true);
        super.pack();
    }

    private void resetField() {
        textFieldNo.setText("");
        textFieldNama.setText("");
        textFieldTelepon1.setText("");
        textFieldTelepon2.setText("");
        anggota = null;
    }
}
