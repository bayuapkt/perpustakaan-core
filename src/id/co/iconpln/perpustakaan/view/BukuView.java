/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.view;

import id.co.iconpln.co.id.entity.Buku;
import id.co.iconpln.perpustakaan.service.ServiceDAO;
import java.awt.Dimension;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Bayu
 */
public class BukuView extends JInternalFrame {

    private JTextField textFieldKode;
    private JTextField textFieldJudul;
    private JTextField textFieldPengarang;
    private JTable table;
    private final List<Buku> bukus = new ArrayList<>();
    private TableBukuViewModel bukuViewModel;
    private ServiceDAO serviceDAO = new ServiceDAO();
    private Buku buku;
    private final ButtonPanel buttonPanel;

    {
        loadAll();
    }

    private void loadAll() {
        bukus.clear();
        bukus.addAll(serviceDAO.findBukus(new Buku()));
    }

    public BukuView(String string) {
        super(string);
        this.buttonPanel = new ButtonPanel() {
            @Override
            public ActionListener onButtonAddClick() {
                return new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (buku != null) {
                            buku.setKode(textFieldKode.getText());
                            buku.setJudul(textFieldJudul.getText());
                            buku.setPengarang(textFieldPengarang.getText());
                            serviceDAO.update(buku);
                            JOptionPane.showMessageDialog(rootPane, "EDIT BERHASIL");
                        } else {
                            buku = new Buku();
                            buku.setKode(textFieldKode.getText());
                            buku.setJudul(textFieldJudul.getText());
                            buku.setPengarang(textFieldPengarang.getText());
                            if (buku.getKode().isEmpty()) {
                                JOptionPane.showMessageDialog(rootPane, "GAGAL, KODE KOSONG");
                            } else {
                                serviceDAO.save(buku);
                                JOptionPane.showMessageDialog(rootPane, "SAVE");
                            }

                        }
                        resetField();
                        loadAll();
                    }
                };
            }

            @Override

            public ActionListener onButtonEditClick() {
                return new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (buku != null) {
                            buku.setKode(textFieldKode.getText());
                            buku.setJudul(textFieldJudul.getText());
                            buku.setPengarang(textFieldPengarang.getText());
                            serviceDAO.update(buku);
                            JOptionPane.showMessageDialog(rootPane, "EDIT BERHASIL");
                        } else {
                            buku = (Buku) bukuViewModel.getValueAt(table.getSelectedRow(), table.getColumnCount());
                            textFieldKode.setText(buku.getKode());
                            textFieldKode.setEditable(false);
                            textFieldJudul.setText(buku.getJudul());
                            textFieldPengarang.setText(buku.getPengarang());
                        }
                    }
                };
            }

            @Override
            public ActionListener onButtonDeleteClick() {
                return new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (buku != null) {
                            buku.setKode(textFieldKode.getText());
                            buku.setJudul(textFieldJudul.getText());
                            buku.setPengarang(textFieldPengarang.getText());
                            serviceDAO.delete(buku);
                            JOptionPane.showMessageDialog(rootPane, "DELETE BERHASIL");
                        } else {
                            buku = (Buku) bukuViewModel.getValueAt(table.getSelectedRow(), table.getColumnCount());
                            textFieldKode.setText(buku.getKode());
                            textFieldKode.setEditable(false);
                            textFieldJudul.setText(buku.getJudul());
                            textFieldPengarang.setText(buku.getPengarang());
                        }
                        loadAll();
                    }
                };
            }
        };
        initComponent();
    }

    private void initComponent() {
        textFieldKode = new JTextField();
        textFieldKode.setMinimumSize(new Dimension(200, 20));

        textFieldJudul = new JTextField();
        textFieldJudul.setMinimumSize(new Dimension(200, 20));

        textFieldPengarang = new JTextField();
        textFieldPengarang.setMinimumSize(new Dimension(200, 20));

        bukuViewModel = new TableBukuViewModel(bukus);

        table = new JTable(bukuViewModel);
        table.setFillsViewportHeight(true);

        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new java.awt.Dimension(450, 150));

        MigLayout layout = new MigLayout("wrap 2");
        JPanel panelform = new JPanel();
        panelform.setLayout(layout);
        panelform.setBorder(new EtchedBorder(1));
        panelform.add(new JLabel("KODE"));
        panelform.add(textFieldKode);
        panelform.add(new JLabel("JUDUL"));
        panelform.add(textFieldJudul);
        panelform.add(new JLabel("PENGARANG"));
        panelform.add(textFieldPengarang);
        panelform.add(buttonPanel, "Span 2, align right");
        panelform.add(scrollPane, "Span 2");

        super.add(panelform);
        super.setPreferredSize(new Dimension(500, 400));
        super.setClosable(true);
        super.setMaximizable(true);
        super.pack();
    }

    private void resetField() {
        textFieldKode.setText("");
        textFieldJudul.setText("");
        textFieldPengarang.setText("");
        buku = null;
    }
}
