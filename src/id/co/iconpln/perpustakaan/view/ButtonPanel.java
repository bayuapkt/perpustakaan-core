/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.view;

import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author Bayu
 */
public abstract class ButtonPanel extends JPanel {

    private final JButton buttonAdd = new JButton("ADD");
    private final JButton buttonEdit = new JButton("EDIT");
    private final JButton buttonDelete = new JButton("DELETE");

    public ButtonPanel() {
        super.add(buttonAdd);
        super.add(buttonEdit);
        super.add(buttonDelete);

        buttonAdd.addActionListener(onButtonAddClick());
        buttonEdit.addActionListener(onButtonEditClick());
        buttonDelete.addActionListener(onButtonDeleteClick());
    }

    public abstract ActionListener onButtonAddClick();

    public abstract ActionListener onButtonEditClick();

    public abstract ActionListener onButtonDeleteClick();
}
