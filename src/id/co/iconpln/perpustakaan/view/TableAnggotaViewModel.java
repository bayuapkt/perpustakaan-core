/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.view;

import id.co.iconpln.co.id.entity.Anggota;
import id.co.iconpln.co.id.entity.Telepon;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Bayu
 */
public class TableAnggotaViewModel extends AbstractTableModel {

    private List<Anggota> anggotas = new ArrayList<>();
    private final String[] columns = {"NO", "NAMA", "Telepon 1", "Telepon 2"};

    public TableAnggotaViewModel(List<Anggota> anggotas) {
        this.anggotas = anggotas;
    }

    @Override
    public int getRowCount() {
        return anggotas.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {

            int sizeTelepon = 0;
            Telepon t[] = new Telepon[2];
            t[0] = new Telepon();
            t[1] = new Telepon();
            t[0].setNo("");
            t[1].setNo("");

            sizeTelepon = anggotas.get(rowIndex).getTelepons().size();
            if (sizeTelepon > 0) {
                Iterator<Telepon> it = anggotas.get(rowIndex).getTelepons().iterator();
                t[0] = it.next();
                t[1] = it.next();
            }

            if (columns[columnIndex].equals("NO")) {
                return anggotas.get(rowIndex).getNo();
            }
            if (columns[columnIndex].equals("NAMA")) {
                return anggotas.get(rowIndex).getNama();
            }
            if (columns[columnIndex].equals("Telepon 1")) {
                return t[0].getNo();
            }
            if (columns[columnIndex].equals("Telepon 2")) {
                return t[1].getNo();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
        }
        return anggotas.get(rowIndex);
    }

    @Override
    public String getColumnName(int column) {
        return columns[column];
    }

}
