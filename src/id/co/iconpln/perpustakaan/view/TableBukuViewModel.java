/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.view;

import id.co.iconpln.co.id.entity.Buku;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Bayu
 */
public class TableBukuViewModel extends AbstractTableModel {

    private List<Buku> bukus = new ArrayList<>();
    private final String[] columns = {"KODE", "JUDUL", "PENGARANG"};

    public TableBukuViewModel(List<Buku> bukus) {
        this.bukus = bukus;
    }

    @Override
    public int getRowCount() {
        return bukus.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            if (columns[columnIndex].equals("KODE")) {
                return bukus.get(rowIndex).getKode();
            }
            if (columns[columnIndex].equals("JUDUL")) {
                return bukus.get(rowIndex).getJudul();
            }
            if (columns[columnIndex].equals("PENGARANG")) {
                return bukus.get(rowIndex).getPengarang();
            }
        } catch (ArrayIndexOutOfBoundsException e) {
        }
        return bukus.get(rowIndex);
    }

    @Override
    public String getColumnName(int column) {
        return columns[column];
    }

}
