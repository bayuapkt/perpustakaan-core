/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao;

import com.sun.org.apache.bcel.internal.generic.PUSH;
import id.co.iconpln.co.id.entity.Buku;
import id.co.iconpln.perpustakaan.dao.impl.BukuDAOImpl;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Bayu
 */
public class BukuDAOTest {

    private BukuDAO bukuDAO;
    private Buku buku;

    @Before
    public void init() {
        bukuDAO = new BukuDAOImpl();

        buku = new Buku();
        buku.setKode("1990");
        buku.setJudul("Belajar Java Dasar");
        buku.setPengarang("Tatang S");
    }

    //@Test
    public void save() {
        Buku result = bukuDAO.save(buku);
        System.out.println(buku);
    }

    @Test
    public void find() {
        List<Buku> result = bukuDAO.find(new Buku());
        for (Buku buku : result) {
            System.out.println(buku);
        }
    }

    //@Test
    public void update() {
        Buku result = bukuDAO.update(buku);
        System.out.println(buku);
    }
    //@Test
    public void delete() {
        Buku result = bukuDAO.delete(buku);
        System.out.println(buku);
    }
}
