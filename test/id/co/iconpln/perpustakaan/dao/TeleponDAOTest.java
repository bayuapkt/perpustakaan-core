/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao;

import id.co.iconpln.co.id.entity.Anggota;
import id.co.iconpln.co.id.entity.Telepon;
import id.co.iconpln.perpustakaan.dao.impl.TeleponDAOImpl;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Bayu
 */
public class TeleponDAOTest {

    private TeleponDAO teleponDAO;
    private Telepon telepon;

    @Before
    public void init() {

        teleponDAO = new TeleponDAOImpl();

        telepon = new Telepon();
        telepon.setNo("081212441780");
        telepon.setAnggota(new Anggota(1));
    }

    @Test
    public void save() {
        Telepon result = teleponDAO.save(telepon);
        System.out.println(result);

    }

}
