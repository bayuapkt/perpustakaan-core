/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao.service;

import id.co.iconpln.co.id.entity.Anggota;
import id.co.iconpln.co.id.entity.Telepon;
import id.co.iconpln.perpustakaan.service.ServiceDAO;
import java.util.HashSet;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Bayu
 */
public class ServiceDAOTest {
    
    private ServiceDAO serviceDAO;
    private Anggota anggota;
    private Telepon telepon;
    
    @Before
    public void init(){
        serviceDAO = new ServiceDAO();
    }
    
    @Test
    public void save(){
        Set<Telepon> telepons = new HashSet<>();
        Telepon telepon1 = new Telepon();
        telepon1.setNo("081212441780");
        telepons.add(telepon1);
        
        Telepon telepon2 = new Telepon();
        telepon2.setNo("081212441780");
        telepons.add(telepon2);
        
        anggota = new Anggota();
        anggota.setNo("123");
        anggota.setNama("Bayu");
        anggota.setTelepons(telepons);
        
        serviceDAO.save(anggota);
    }
}
